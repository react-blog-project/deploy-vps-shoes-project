# shoes-shop-deploy to VPS

## Description
[Setup ubuntu first time](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-18-04)
[Docker](https://docs.docker.com/engine/install/centos) install docker on your VPS.
[Docker Compose](https://docs.docker.com/compose/install) install docker on your VPS.
[Gitlab Runner](https://docs.gitlab.com/runner/install/) install gitlab runner

## Setup code in the local
### Let's open terminal and use this command line to clone the code
```bash
cd ~
mkdir shoes-shop-project && cd $_
git clone git@gitlab.com:react-blog-project/deploy-vps-shoes-project.git
git clone git@gitlab.com:react-blog-project/nodejs-shoes-shop-project.git
git clone git@gitlab.com:react-blog-project/react-shoes-shop-project.git
```

### If you are running code or postgres container before, you should run stop all
```bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker image rm {image shoes_shop_api}
docker image rm {image shoes_shop_admin}
```

### Config env for every code
```bash
cd ~/shoes-shop-project/nodejs-shoes-shop-project
cp .env.dev .env

cd ~/shoes-shop-project/react-shoes-shop-project
cp .env.dev .env
```
### Running all code with command line
```bash
cd ~/shoes-shop-project/deploy-vps-shoes-project
docker-compose -f docker-compose-local.yml up # -d if you want to run background mode
```

### Add file hosts in ubuntu/mac
```bash
sudo nano /etc/hosts
```
Update content to `127.0.0.1 localhost local.shoeshop.com www.local.shoeshop.com`

## Here's a List of Docker Commands
```bash
docker ps -a # list all containers
docker logs -f {container id} # check logs every container
docker restart {container id} # restart container when change config code 
```

## Docker Develop

```bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker image rm {image shoes_shop_api}
docker image rm {image shoes_shop_admin}
docker-compose up -f docker-compose-dev.yml
```

## Docker Staging

```bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker image rm {image shoes_shop_api}
docker image rm {image shoes_shop_admin}
docker-compose up -d
```

## Account Docker
```bash
$ DOCKER_USERNAME: shoesshop123adm
$ DOCKER_PASSWORD: shoesshop@123
```

## Login VPS dev
```bash
$ ssh root@171.244.21.245
$ {input password}
$ cd /opt/deploy
$ docker-compose pull
$ docker-compose up -d
$ docker restart {container name} # just run when update nginx
```

## Config SSL support
```bash
SSL Support: https://github.com/nginx-proxy/nginx-proxy#ssl-support
```