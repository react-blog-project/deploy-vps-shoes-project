set -x

cd /var/www/html

# run package first time
if [ ! -f /tmp/container-initialized ]; then
    # clear cache npm
    npm cache clean --force
    npm cache ls
    rm -rf ~/.npm
    # install global package
    yarn global add @nestjs/cli
    yarn global add npm-check-updates
fi

# Because package-lock.json is versioned, npm instead of yarn
npm install

# Check update of package.json only at the first execution
if [ ! -f /tmp/container-initialized ]; then
    ncu

    touch /tmp/container-initialized
fi

npm run start:dev
